<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TecnologiaRepository")
 * @ORM\Table(name="tecnologias")
 */
class Tecnologia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $nome;

    /**
     * @var mixed
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Candidato", mappedBy="tecnologias")
     */
    private $candidatos;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return Tecnologia
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCandidatos()
    {
        return $this->candidatos;
    }

    /**
     * @param mixed $candidatos
     * @return Tecnologia
     */
    public function setCandidatos($candidatos)
    {
        $this->candidatos = $candidatos;
        return $this;
    }
}
