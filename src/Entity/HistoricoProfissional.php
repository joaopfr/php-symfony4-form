<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HistoricoProfissionalRepository")
 * @ORM\Table(name="historicos_profissional")
 */
class HistoricoProfissional
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $nome_empresa;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $emprego_atual = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $data_entrada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     *
     * @Assert\Expression(
     *     "this.isEmpregoAtual() === true && value === null ||  this.isEmpregoAtual() === false && value !== null",
     *     message="Se for seu emprego atual o campo data saída deve ser vazio"
     * )
     * @Assert\Date()
     */
    private $data_saida;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidato", inversedBy="historico")
     */
    private $candidato;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNomeEmpresa()
    {
        return $this->nome_empresa;
    }

    /**
     * @param mixed $nome_empresa
     * @return HistoricoProfissional
     */
    public function setNomeEmpresa($nome_empresa)
    {
        $this->nome_empresa = $nome_empresa;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataEntrada()
    {
        return $this->data_entrada;
    }

    /**
     * @param mixed $data_entrada
     * @return HistoricoProfissional
     */
    public function setDataEntrada($data_entrada)
    {
        $this->data_entrada = $data_entrada;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataSaida()
    {
        return $this->data_saida;
    }

    /**
     * @param mixed $data_saida
     * @return HistoricoProfissional
     */
    public function setDataSaida($data_saida)
    {
        $this->data_saida = $data_saida;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEmpregoAtual(): bool
    {
        return $this->emprego_atual;
    }

    /**
     * @param bool $emprego_atual
     * @return HistoricoProfissional
     */
    public function setEmpregoAtual(bool $emprego_atual): HistoricoProfissional
    {
        $this->emprego_atual = $emprego_atual;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCandidato()
    {
        return $this->candidato;
    }

    /**
     * @param mixed $candidato
     * @return HistoricoProfissional
     */
    public function setCandidato($candidato)
    {
        $this->candidato = $candidato;
        return $this;
    }
}
